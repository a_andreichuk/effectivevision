from collections import deque
from collections import namedtuple
import numpy as np
import cv2

from ..engine.base_service import BaseService
from ..engine.adapters import get as get_adapter
from ..utils.generic_utils import log
from ..utils.output_utils import draw_objects_np
from ..utils.output_utils import label_to_color_image
from ..utils.output_utils import draw_tracking_sparse


class EmptyService(BaseService):
    def __init__(self, service, *args, **kwargs):
        super().__init__(
                adapter=get_adapter('simple')([service]),
                *args, **kwargs)

    def _generator(self):
        while True:
            yield self._get_inputs(0)


class PerformanceBar(BaseService):
    BacklogItem = namedtuple('BacklogItem', 'timestamp delay')

    def __init__(self, service, n_frames=200, *args, **kwargs):
        self.n_frames = n_frames
        super().__init__(
                adapter=get_adapter('simple')({'service': service}),
                *args, **kwargs)

    def _generator(self):
        total_delay = 0.
        frame_counter = 0
        start_time = None

        if self.n_frames > 0:
            performance_log = deque()

        while True:
            image = self._get_inputs('service')
            assert isinstance(image, np.ndarray) and len(image.shape) == 3 \
                and image.shape[2] == 3 and image.dtype == np.uint8

            delay = self._history_tape[-1].timestamp - \
                self._history_tape[0].timestamp

            if self.n_frames > 0:
                performance_log.append(PerformanceBar.BacklogItem(
                    self._history_tape[-1].timestamp, delay))
                if len(performance_log) > self.n_frames:
                    el = performance_log.popleft()
                    total_delay -= el.delay
                if len(performance_log) == 1:
                    continue
            else:
                frame_counter += 1
                if start_time is None:
                    start_time = self._history_tape[-1].timestamp
                    continue
            total_delay += delay

            if self.n_frames > 0:
                lst = list(performance_log)
                avg_fps = len(performance_log) / (
                        lst[-1].timestamp - lst[0].timestamp + 1e-6)
                avg_delay = total_delay / len(performance_log)
            else:
                now = self._history_tape[-1].timestamp
                avg_fps = float(frame_counter) / (now - start_time)
                avg_delay = total_delay / frame_counter

            tx_color = (255, 255, 255)
            bg_color = (0, 0, 0)
            thickness = 1
            padding = (5, 5)
            font_scale = 0.6
            font = cv2.FONT_HERSHEY_DUPLEX

            text = 'FPS: {:.1f} | Delay: {:.1f}ms'.format(
                    avg_fps, avg_delay * 1000.)
            (tw, th), baseline = cv2.getTextSize(
                    text, font, fontScale=font_scale, thickness=thickness)
            if tw > image.shape[1]:
                log.warning(
                    'Status bar is too wide for image ({} > {})'.format(
                        tw, image.shape[1]))

            xmin, ymax = padding[0], th + 2 * padding[1]
            display_img = np.empty(
                    shape=(
                        image.shape[0] + th + 2 * padding[1],
                        image.shape[1], 3),
                    dtype=np.uint8)
            display_img[:th + 2 * padding[1], :, :] = np.array(bg_color)
            display_img[th + 2 * padding[1]:, :, :] = image
            cv2.putText(display_img, text,
                        (xmin + padding[0], ymax - padding[1]),
                        font, fontScale=font_scale,
                        color=tx_color, thickness=thickness,
                        lineType=cv2.LINE_AA)

            yield display_img


class DetectronDraw(BaseService):
    def __init__(self,
                 image_stream,
                 detector=None,
                 segmentator=None,
                 tracker=None,
                 contours=None):

        self._has_detector = False
        self._has_segmentator = False
        self._has_tracker = False
        self._has_contours = False
        input_services = {'image_stream': image_stream}

        if detector is not None:
            self._has_detector = True
            input_services['detector'] = detector

        if segmentator is not None:
            self._has_segmentator = True
            input_services['segmentator'] = segmentator

        if tracker is not None:
            self._has_tracker = True
            input_services['tracker'] = tracker

        if contours is not None:
            self._has_contours = True
            input_services['contours'] = contours

        super().__init__(adapter=get_adapter('sync')(input_services))

    def _generator(self):
        while True:
            ret_val = self._get_inputs(
                    'image_stream', 'detector', 'segmentator', 'tracker',
                    'contours')
            image, detections, segmentation, tracking, contours = ret_val
            image = self._safe_resolve_input(image, readonly=False)

            if image is None:
                log.warning(
                        '`image_stream` yielded None (expected behavior), '
                        'continue.')
                continue

            assert isinstance(image, np.ndarray) and len(image.shape) == 3 \
                and image.shape[2] == 3 and image.dtype == np.uint8

            if self._has_segmentator and segmentation is not None:
                assert isinstance(segmentation, np.ndarray) and \
                    len(segmentation.shape) == 2

                vis_res = label_to_color_image(
                        segmentation.astype(np.int)).astype(np.uint8)
                if vis_res.shape != image.shape:
                    vis_res = cv2.resize(
                            vis_res, (image.shape[1], image.shape[0]))
                image = 2 * (vis_res // 3) + image // 3

            if self._has_detector and detections is not None:
                objects, labels = detections
                draw_objects_np(image, objects, labels)

            if self._has_tracker and tracking is not None:
                prev_points, cur_points = tracking
                draw_tracking_sparse(image, prev_points, cur_points)

            if self._has_contours and contours is not None:
                cv2.drawContours(image, contours, -1, (0, 255, 255), 2)

            yield image
