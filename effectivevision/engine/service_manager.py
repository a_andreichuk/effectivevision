from abc import ABCMeta
import time

from .base_service import BaseService
from ..utils.generic_utils import log
from ..utils.generic_utils import class_name


class ServiceManager(metaclass=ABCMeta):
    def __init__(self, *services):
        for service in services:
            self.add_recursive(service)

    def add(self, service, name=None):
        if name is None:
            base_name = type(service).__name__
            i = 1
            ith_name = base_name + str(i)
            while hasattr(self, ith_name):
                i += 1
                ith_name = base_name + str(i)
            name = ith_name

        if hasattr(self, name):
            raise ValueError('Attribute "{}" already exists.'.format(name))
        setattr(self, name, service)
        self._register(name)
        return service

    def add_recursive(self, service):
        self.add(service)
        for _, child_service in service._input_adapter._input_services.items():
            self.add_recursive(child_service)
        return service

    def start(self):
        services = self.enlist_services()
        for service in services:
            service.start()

    def stop(self, warning_interval=10, rage_resign_threshold=-1):
        services = self.enlist_services()
        begin = time.time()
        warning_threshold = warning_interval

        for service in services:
            service.stop()

        while True:
            all_stopped = True
            not_stopped = []
            for service in services:
                if service._thread is not None:
                    all_stopped = False
                    not_stopped.append(class_name(service))

            if not all_stopped:
                diff = time.time() - begin
                if diff > warning_threshold:
                    log.warning(
                            'The following services are not responding after '
                            '{:d} seconds: {}'.format(
                                warning_interval, ', '.join(not_stopped)))

                if diff > rage_resign_threshold and rage_resign_threshold >= 0:
                    log.warning('Resign on wairning for services to stop.')
                    return False
                time.sleep(0.01)
            else:
                break
        return True

    def enlist_services(self):
        services = []
        for attr_name in dir(self):
            attr = getattr(self, attr_name)
            if isinstance(attr, BaseService):
                services.append(attr)
        return services

    def _register(self, name):
        if not hasattr(self, name):
            raise KeyError('No service with name {}'.format(name))
        if not isinstance(getattr(self, name), BaseService):
            raise ValueError('The attribute "{}" is not a service'
                             .format(name))
        service = getattr(self, name)
        service.manager = self
