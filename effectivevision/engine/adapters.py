from abc import ABCMeta, abstractmethod
from collections import deque
import threading
import time
from ..utils.generic_utils import log


empty_query = ([], None)


class BaseInputAdapter(metaclass=ABCMeta):
    def __init__(self, input_services=dict()):
        if isinstance(input_services, list):
            self._input_services = dict(enumerate(input_services))
        elif isinstance(input_services, dict):
            self._input_services = input_services
        else:
            raise ValueError(
                    '`input_services` should either be a dict or a list')

    def get_inputs(self, *input_ids):
        if len(input_ids) == 0:
            return empty_query

        existing_ids = [
                sid for sid in input_ids if sid in self._input_services]

        if len(existing_ids) == 0:
            return tuple(empty_query for _ in input_ids)

        output_dict = self._get_inputs_internal(*existing_ids)
        ret = tuple(
                output_dict[sid] if sid in output_dict else empty_query
                for sid in input_ids)

        return ret[0] if len(ret) == 1 else ret

    def start(self):
        pass

    def stop(self, wait=False):
        pass

    @abstractmethod
    def _get_inputs_internal(self, *input_ids):
        return NotImplemented


class SimpleAdapter(BaseInputAdapter):
    def __init__(self, input_services):
        super().__init__(input_services)

    def _get_inputs_internal(self, *input_ids):
        return dict((input_id, self._input_services[input_id].query())
                    for input_id in input_ids)


class LatestAdapter(BaseInputAdapter):
    def __init__(self, input_services):
        super().__init__(input_services)
        self._saved_outputs = dict(
                (input_id, None)
                for input_id in input_services.keys())
        self._threads = dict(
                (input_id, None)
                for input_id in input_services.keys())
        self._updated_event = threading.Event()
        self._stopped = True

    def start(self):
        self._stopped = False
        for input_id in self._input_services:
            if self._threads[input_id] is None:
                self._threads[input_id] = threading.Thread(
                        target=self._update_cache, args=(input_id, ))
                self._threads[input_id].start()

    def stop(self, wait=False):
        self._stopped = True
        if wait and len(self._threads) > 0:
            for _, thread in self._threads.items():
                if thread is not None:
                    thread.join()

    def _update_cache(self, input_id):
        while True:
            if self._stopped:
                break
            self._saved_outputs[input_id] = \
                self._input_services[input_id].query()
            self._updated_event.set()

        self._threads[input_id] = None

    def _get_inputs_internal(self, *input_ids):
        if self._stopped:
            log.error('The adapter is stopped, cannot retrieve anything')
            return dict((input_id, empty_query) for input_id in input_ids)

        self._updated_event.wait()
        self._updated_event.clear()
        ret = dict((input_id, empty_query) for input_id in input_ids)
        for input_id in input_ids:
            val = self._saved_outputs[input_id]
            if val is not None:
                ret[input_id] = val
        return ret


class CachedAdapter(BaseInputAdapter):
    def __init__(self, input_services):
        super().__init__(input_services)
        self._saved_outputs = dict(
                (input_id, None)
                for input_id in input_services.keys())
        self._threads = dict(
                (input_id, None)
                for input_id in input_services.keys())
        self._stopped = True

    def start(self):
        self._stopped = False
        for input_id in self._input_services:
            if self._threads[input_id] is None:
                self._threads[input_id] = threading.Thread(
                        target=self._update_cache, args=(input_id, ))
                self._threads[input_id].start()

    def stop(self, wait=False):
        self._stopped = True
        if wait and len(self._threads) > 0:
            for _, thread in self._threads.items():
                if thread is not None:
                    thread.join()

    def _update_cache(self, input_id):
        while True:
            if self._stopped:
                break
            self._saved_outputs[input_id] = \
                self._input_services[input_id].query()

        self._threads[input_id] = None

    def _get_inputs_internal(self, *input_ids):
        if self._stopped:
            log.error('The adapter is stopped, cannot retrieve anything')
            return dict((input_id, empty_query) for input_id in input_ids)

        ret = dict((input_id, empty_query) for input_id in input_ids)
        for input_id in input_ids:
            val = self._saved_outputs[input_id]
            if val is not None:
                ret[input_id] = val
        return ret


class SyncAdapter(BaseInputAdapter):
    def __init__(self,
                 input_services,
                 max_cache_size=200,
                 adjust_slowest=True,
                 fps_cache=50):

        super().__init__(input_services)
        self._cache = dict(
                (service_name, deque(maxlen=max_cache_size))
                for service_name in input_services.keys())

        self._threads = dict(
                (input_id, None)
                for input_id in input_services)

        self._adjust_slowest = adjust_slowest
        if adjust_slowest:
            assert fps_cache > 1
            self._fps_cache = fps_cache
            self._fps = dict((input_id, 0) for input_id in input_services)

        self._received_event = threading.Event()
        self._stopped = True

    def start(self):
        self._stopped = False
        for input_id in self._input_services:
            if self._threads[input_id] is None:
                if self._adjust_slowest:
                    self._fps[input_id] = 0
                self._threads[input_id] = threading.Thread(
                        target=self._update_inputs, args=(input_id, ))
                self._threads[input_id].start()

    def stop(self, wait=False):
        self._stopped = True
        if wait and len(self._threads) > 0:
            for _, thread in self._threads.items():
                if thread is not None:
                    thread.join()

    def _update_inputs(self, input_id):
        if self._adjust_slowest:
            start_time = time.time()
            backlog = deque(maxlen=self._fps_cache)

        while True:
            if self._stopped:
                break

            last = self._input_services[input_id].query()

            if self._adjust_slowest:
                backlog.append(time.time())
                if len(backlog) == backlog.maxlen:
                    self._fps[input_id] = len(backlog) / (
                            backlog[-1] - backlog[0])
                else:
                    self._fps[input_id] = len(backlog) / (
                            backlog[-1] - start_time)

            cache = self._cache[input_id]
            cache.append(last)
            if len(cache) == cache.maxlen:
                log.warning('Cache for `{}` reached max capacity of {}'
                            .format(input_id, cache.maxlen))

            if self._adjust_slowest:
                min_fps = min(self._fps.values())
                if not self._fps[input_id] == min_fps:
                    continue
            self._received_event.set()

        self._threads[input_id] = None

    def _get_inputs_internal(self, *input_ids):
        if self._stopped:
            log.error('The adapter is stopped, cannot retrieve anything')
            return dict((input_id, empty_query) for input_id in input_ids)

        self._received_event.wait()
        self._received_event.clear()
        ret = dict()

        earliest = None
        for input_id in input_ids:
            cache = self._cache[input_id]
            if len(cache) == 0:
                continue

            t = cache[-1][0][0].timestamp
            if earliest is None:
                earliest = t
            else:
                earliest = min(t, earliest)

        for input_id in input_ids:
            cache = self._cache[input_id]
            if len(cache) == 0:
                ret[input_id] = empty_query
                continue

            ret[input_id] = cache[-1]

            while len(cache) > 1 and \
                    cache[0][0][0].timestamp < earliest:
                ret[input_id] = cache.popleft()

            d0 = abs(ret[input_id][0][0].timestamp - earliest)
            d1 = abs(cache[0][0][0].timestamp - earliest)
            if d1 < d0:
                if len(cache) > 1:
                    ret[input_id] = cache.popleft()
                else:
                    ret[input_id] = cache[0]

        return ret


class MultiAdapter(BaseInputAdapter):
    def __init__(self, *input_adapters):
        services = dict()
        for adapter in input_adapters:
            services.extend(adapter._input_services)
        self._adapters = input_adapters
        super().__init__(services)

    def _get_inputs_internal(self, *input_ids):
        ret_dict = dict()
        for adapter in self._input_adapters:
            sub_input_ids = list(set(input_ids).union(
                set(adapter._input_services.keys())))
            if len(sub_input_ids) > 0:
                sub_inputs = adapter.get_input(*sub_input_ids)
                ret_dict.extend(dict(zip(sub_input_ids, sub_inputs)))
        return ret_dict


def get(identifier):
    if isinstance(identifier, str):
        if identifier == 'simple':
            return SimpleAdapter
        elif identifier == 'latest':
            return LatestAdapter
        elif identifier == 'sync':
            return SyncAdapter
        elif identifier == 'cached':
            return CachedAdapter
    elif isinstance(identifier, BaseInputAdapter):
        return identifier
    else:
        raise ValueError('Could not interpret '
                         'adapter identifier:', identifier)
