import colorlog
import logging
import inspect
import shutil
import wget
import sys
import os


class Log:
    aliases = {
        logging.CRITICAL: ("critical", "crit", "c", "fatal"),
        logging.ERROR:    ("error", "err", "e"),
        logging.WARNING:  ("warning", "warn", "w"),
        logging.INFO:     ("info", "inf", "nfo", "i"),
        logging.DEBUG:    ("debug", "dbg", "d")
    }

    lvl = logging.DEBUG
    format_str = "%(log_color)s%(asctime)s | %(levelname)-8s | " \
                 "%(message)s (%(filename)s:%(lineno)d)%(reset)s"
    logging.root.setLevel(lvl)
    formatter = colorlog.ColoredFormatter(
            format_str, datefmt="%H:%M:%S", reset=True,
            log_colors={
                'DEBUG': 'cyan',
                'INFO': 'reset',
                'WARNING': 'yellow',
                'ERROR': 'red',
                'CRITICAL': 'bold_red',
            })

    stream = logging.StreamHandler()
    stream.setLevel(lvl)
    stream.setFormatter(formatter)
    logger = logging.getLogger('effectivevision')
    logger.setLevel(lvl)
    logger.addHandler(stream)

    crit = c = fatal = critical = logger.critical
    err = e = error = logger.error
    warning = w = warn = logger.warning
    inf = nfo = i = info = logger.info
    dbg = d = debug = logger.debug

    @classmethod
    def _parse_level(cls, lvl):
        for log_level in cls.aliases:
            if lvl == log_level or lvl in cls.aliases[log_level]:
                return log_level
        raise TypeError("Unrecognized logging level: %s" % lvl)

    @classmethod
    def level(cls, lvl=None):
        '''Get or set the logging level.'''
        if not lvl:
            return cls._lvl
        cls._lvl = cls._parse_level(lvl)
        cls.stream.setLevel(cls._lvl)
        logging.root.setLevel(cls._lvl)


log = Log()
if 'VERBOSE' in os.environ:
    log.level(os.environ['VERBOSE'])


class Resource:
    def __init__(self, collection_name, url, is_archive=False, root_dir=None):

        if root_dir is None:
            if 'EFFECTIVEVISIONDIR' in os.environ:
                root_dir = os.environ['EFFECTIVEVISIONDIR']
            else:
                root_dir = '~/.effectivevision/'
        root_dir = os.path.expanduser(root_dir)
        if not os.path.isdir(root_dir):
            os.makedirs(root_dir)

        collection_dir = os.path.join(root_dir, collection_name)
        if not os.path.isdir(collection_dir):
            os.makedirs(collection_dir)

        self._path = self.retrieve(url, collection_dir, is_archive)

    def path(self):
        return self._path

    @staticmethod
    def retrieve(url, folder, is_archive=False):
        file_name = os.path.basename(url)
        download_path = os.path.join(folder, file_name)

        if is_archive:
            formats = [".zip", ".tar", ".gztar", ".bztar", ".xztar"]
            archive_format = None
            for f in formats:
                if download_path[-len(f):] == f:
                    archive_format = f

            if archive_format is None:
                raise RuntimeError('Only [{}] formats are supported.'.format(
                    ', '.join(formats)))

            dirname = download_path.rsplit(archive_format, 1)[0]
            if not os.path.isdir(dirname):
                wget.download(url, download_path)
                shutil.unpack_archive(
                        download_path, os.path.join(folder, dirname))
                os.remove(download_path)
            return dirname

        else:
            if not os.path.exists(download_path):
                wget.download(url, download_path)
            return download_path


def _parent_frame(skip=1):
    def stack_(frame):
        framelist = []
        while frame:
            framelist.append(frame)
            frame = frame.f_back
        return framelist

    stack = stack_(sys._getframe(1))
    start = 0 + skip
    if len(stack) < start + 1:
        return ''
    return stack[start]


def caller_name(skip=1):
    parentframe = _parent_frame(skip)

    name = []
    module = inspect.getmodule(parentframe)
    if module:
        name.append(module.__name__)
    if 'self' in parentframe.f_locals:
        name.append(parentframe.f_locals['self'].__class__.__name__)
    codename = parentframe.f_code.co_name
    if codename != '<module>':  # top level usually
        name.append(codename)  # function or a method
    del parentframe
    return ".".join(name)


def caller_class_name(skip=1):
    parentframe = _parent_frame(skip)
    name = []
    module = inspect.getmodule(parentframe)
    if module:
        name.append(module.__name__)
    if 'self' in parentframe.f_locals:
        name.append(parentframe.f_locals['self'].__class__.__name__)
    del parentframe
    return ".".join(name)


def class_name(obj):
    return '.'.join([type(obj).__module__, type(obj).__name__])
