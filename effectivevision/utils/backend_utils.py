import os
import yaml
import platform
import importlib

from .generic_utils import log

_effectivevision_base_dir = os.path.expanduser('~')
if not os.access(_effectivevision_base_dir, os.W_OK):
    _effectivevision_base_dir = '/tmp'
_effectivevision_dir = os.path.join(_effectivevision_base_dir, '.effectivevision')
_effectivevision_dir = os.environ.get('EFFECTIVEVISION_HOME', _effectivevision_dir)
if not os.path.isdir(_effectivevision_dir):
    os.makedirs(_effectivevision_dir)
log.debug('[Init] EffectiveVision dir: {}'.format(_effectivevision_dir))

_backend = 'tflite'

_mode = 'debug'

_USING_TFLITE_RUNTIME = False
_USING_TENSORFLOW_TFLITE = False
_USING_EDGE_TPU = False
_USING_RASPBERRYPI_CAMERA = False
_EDGETPU_SHARED_LIB = None

_config_path = os.path.expanduser(
        os.path.join(_effectivevision_dir, 'effectivevision.yaml'))
if os.path.exists(_config_path):
    try:
        with open(_config_path) as stream:
            _config = yaml.safe_load(stream)
    except ValueError:
        _config = dict()
    _backend = _config.get('backend', _backend)
    _mode = _config.get('mode', _mode)
else:
    _backend = os.environ.get('EFFECTIVEVISION_BACKEND', _backend)
    _mode = os.environ.get('EFFECTIVEVISION_MODE', _mode)
log.debug('[Init] Backend is set to {}'.format(_backend))
log.debug('[Init] Mode is set to {}'.format(_mode))

if _backend == 'tflite':
    try:
        import tflite_runtime.interpreter as tflite
        _USING_TFLITE_RUNTIME = True
        log.debug('[Init] Using `tflite` from `tflite_runtime`.')
    except (ImportError, ModuleNotFoundError):
        try:
            import tensorflow.lite as tflite
            _USING_TENSORFLOW_TFLITE = True
            log.debug('[Init] Using `tflite` from `tensorflow`.')
        except (ImportError, ModuleNotFoundError) as e:
            raise ImportError(
                'Backend "tflite" not found. Please make sure that either'
                'tensorflow or tflite_runtime is installed') from e

    _EDGETPU_SHARED_LIB = {
        'Linux': 'libedgetpu.so.1',
        'Darwin': 'libedgetpu.1.dylib',
        'Windows': 'edgetpu.dll'
    }[platform.system()]

   # try:
   #     if _USING_TENSORFLOW_TFLITE:
   #         tflite.experimental.load_delegate(_EDGETPU_SHARED_LIB)
   #     elif _USING_TFLITE_RUNTIME:
   #         tflite.load_delegate(_EDGETPU_SHARED_LIB)
   #     _USING_EDGE_TPU = True
   #     log.debug('[Init] Using Google Coral Edge TPU.')
   # except (ValueError, RuntimeError):
   #     _EDGETPU_SHARED_LIB = None

else:
    raise ValueError('Backend `{}` not supported.'.format(_backend))

try:
    picam_spec = importlib.util.find_spec('picamera')
    if picam_spec is not None:
        log.debug('[Init] Found picamera module.')
        _USING_RASPBERRYPI_CAMERA = True
except ImportError:
    _USING_RASPBERRYPI_CAMERA = False
