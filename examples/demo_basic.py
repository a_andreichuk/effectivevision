from flask import Flask, render_template, Response
import cv2

import effectivevision
import effectivevision.services as services


def construct_effectivevision_pipeline():
    # camera = services.PiCamera()
    camera = services.VideoFileReader()

    detector = services.ObjectDetector(camera)
    segmentator = services.SemanticSegmentator(camera)
    tracker = services.OpticalFlowLucasKanade(camera)
    contour_extrapolator = services.SegmentationExtrapolator(
            segmentator=segmentator, tracker=tracker, orig_size=(640, 480))

    composer = services.DetectronDraw(
        image_stream=camera,
        # detector=detector,
        # segmentator=segmentator,
        tracker=tracker,
        contours=contour_extrapolator,
    )
    return services.PerformanceBar(composer)


app = Flask(__name__)

service = construct_effectivevision_pipeline()
manager = effectivevision.ServiceManager(service)
manager.start()


@app.route('/')
def index():
    return render_template('index.html')


def gen(camera):
    while True:
        frame = cv2.cvtColor(camera.query()[1], cv2.COLOR_RGB2BGR)
        frame = cv2.imencode('.jpg', frame)[1].tobytes()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')


@app.route('/video_feed')
def video_feed():
    return Response(gen(service),
                    mimetype='multipart/x-mixed-replace; boundary=frame')


if __name__ == '__main__':
    app.run(host='0.0.0.0', threaded=True)
